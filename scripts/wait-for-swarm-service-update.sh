#!/bin/bash

SERVICE_NAME="${1}"
TIMEOUT=${2:-120}

if [[ -z ${SERVICE_NAME} ]]; then
    printf "You have to pass the service name as the first command line argument.\n"
    exit 1
fi

printf "Service name:   %s\n" "${SERVICE_NAME}"
printf "Script timeout: %ds\n" "${TIMEOUT}"

UPDATE_CHECK=$(docker service inspect "${SERVICE_NAME}" -f "{{.UpdateStatus}}")

# shellcheck disable=SC2181
if [[ ${?} -ne 0 ]]; then
    printf "Service does not exists.\n"
    exit 1
elif [[ ${UPDATE_CHECK} == "<nil>" ]]; then
    printf "Service was created initially.\nThis script only supports service updates.\n"
    exit 0
fi

set -eu

DATA=$(docker service inspect "${SERVICE_NAME}" -f "{{.UpdateStatus.StartedAt}}|{{ .PreviousSpec.TaskTemplate.ContainerSpec.Image }}|{{ .Spec.TaskTemplate.ContainerSpec.Image }}")
STARTED_AT=$(echo "${DATA}" | cut -d'|' -f1)
PREVIOUS_IMAGE=$(echo "${DATA}" | cut -d'|' -f2)
TARGET_IMAGE=$(echo "${DATA}" | cut -d'|' -f3)

printf "Update started at %s\n" "${STARTED_AT}"
printf "Previous image: %s\n" "${PREVIOUS_IMAGE}"
printf "Target image:   %s\n" "${TARGET_IMAGE}"

I=0

printf "Updating app."
while true; do
    STATE=$(docker service inspect "${SERVICE_NAME}" -f "{{.UpdateStatus.State}}")

    if [[ ${STATE} == "updating" ]]; then
        I=$((I + 1))
        if [[ ${I} -gt ${TIMEOUT} ]]; then
            printf "Timeout of %s seconds exceeded: abort script\n" "${I}"
            exit 1
        fi

        if [ $((I % 15)) -eq 0 ]; then
            printf "%ds" "${I}"
        else
            printf "."
        fi
        sleep 1

    elif [[ ${STATE} == "completed" ]]; then
        COMPLETED_AT=$(docker service inspect "${SERVICE_NAME}" -f "{{.UpdateStatus.CompletedAt}}")
        printf "\e[32mdone\e[0m\n"
        printf "Update completed at %s\n" "${COMPLETED_AT}"
        exit 0

    else
        MESSAGE=$(docker service inspect "${SERVICE_NAME}" -f "{{.UpdateStatus.Message}}")
        printf "\e[31merror\e[0m\n"
        printf "Update stops with state \"%s\". Message: %s" "${STATE}" "${MESSAGE}"
        exit 1
    fi
done
