ARG BINARY_NAME

############################
# STEP 1 build executable binary
############################
FROM golang:1.24.1@sha256:fa145a3c13f145356057e00ed6f66fbd9bf017798c9d7b2b8e956651fe4f52da AS builder

ARG BINARY_NAME
WORKDIR ${GOPATH}/src/${BINARY_NAME}

RUN apt-get install -y --no-install-recommends git ca-certificates && \
    mkdir -p ~/.ssh && \
    touch ~/.ssh/known_hosts && \
    ssh-keyscan gitlab.com > ~/.ssh/known_hosts

COPY . .

ARG CI_JOB_TOKEN=""
ENV CI=true
RUN make build && \
    make check-licenses > /dev/null

############################
# STEP 2 build a small image
############################
FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

RUN apk --no-cache update && \
    apk --no-cache add curl && \
    mkdir /app

WORKDIR /app

ARG BINARY_NAME
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/${BINARY_NAME}/${BINARY_NAME} /app/
COPY --from=builder /go/src/${BINARY_NAME}/LICEN* /app/
COPY --from=builder /go/src/${BINARY_NAME}/README.md /app/

RUN ln -s /app/${BINARY_NAME} /app/app

HEALTHCHECK --interval=10s \
            --timeout=10s \
            --start-period=5s \
            CMD curl -f http://127.0.0.1:4242/status || exit 1

EXPOSE 4242

ENTRYPOINT ["./app"]
