package main

import (
	"crypto/md5"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/bcrypt"
	"syreclabs.com/go/faker"
)

//nolint:paralleltest
func TestUserByToken(t *testing.T) {
	pw1, pw2 := genAndSetTestUser(t)

	t.Run("check pw1", func(t *testing.T) {
		u, found := userByToken(fmt.Sprintf("%s:%s", config.Users[0].Name, pw1))
		require.True(t, found)
		require.Equal(t, config.Users[0], u)
	})

	t.Run("check pw1 with prefix", func(t *testing.T) {
		u, found := userByToken(fmt.Sprintf("at_%s:%s", config.Users[0].Name, pw1))
		require.True(t, found)
		require.Equal(t, config.Users[0], u)
	})

	t.Run("check pw2", func(t *testing.T) {
		u, found := userByToken(fmt.Sprintf("%s:%s", config.Users[1].Name, pw2))
		require.True(t, found)
		require.Equal(t, config.Users[1], u)
	})

	t.Run("check unknown pw", func(t *testing.T) {
		u, found := userByToken(faker.Lorem().Word())
		require.False(t, found)
		require.Equal(t, user{}, u)
	})

	t.Run("check wrong user-pw combination", func(t *testing.T) {
		u, found := userByToken(fmt.Sprintf("%s:%s", config.Users[0].Name, pw2))

		require.False(t, found)
		require.Equal(t, user{}, u)
	})

	t.Run("check too many token parts", func(t *testing.T) {
		u, found := userByToken(fmt.Sprintf("%s:%s:hello", config.Users[1].Name, pw2))
		require.False(t, found)
		require.Equal(t, user{}, u)
	})

	t.Run("check token cache", func(t *testing.T) {
		pw3 := faker.Lorem().Word()
		u := user{
			Name:           faker.Lorem().Word(),
			RequiredPrefix: faker.Lorem().Word(),
		}
		token := fmt.Sprintf("%s:%s", u.Name, pw3)
		tokenCache[md5.Sum([]byte(token))] = u

		actual, found := userByToken(token)
		require.True(t, found)
		require.Equal(t, u, actual)
	})
}

func genAndSetTestUser(t *testing.T) (string, string) {
	pw1 := faker.Lorem().Word()
	pw2 := faker.Lorem().Word()
	pwHash1, err := bcrypt.GenerateFromPassword([]byte(pw1), 12)
	require.Nil(t, err)
	pwHash2, err := bcrypt.GenerateFromPassword([]byte(pw2), 12)
	require.Nil(t, err)

	config.Users = []user{
		{
			Name:           faker.Lorem().Word(),
			TokenHash:      string(pwHash1),
			RequiredPrefix: faker.Lorem().Word(),
		},
		{
			Name:      faker.Lorem().Word(),
			TokenHash: string(pwHash2),
		},
	}

	return pw1, pw2
}
