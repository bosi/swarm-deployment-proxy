package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"syreclabs.com/go/faker"
)

func TestAllow(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest(http.MethodGet, "/", nil)
	require.Nil(t, err)
	require.Nil(t, allow(echo.New().NewContext(req, httptest.NewRecorder())))
}

func TestDeny(t *testing.T) {
	t.Parallel()

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	require.NotNil(t, deny(echo.New().NewContext(req, httptest.NewRecorder())))
}

func TestPrefixNameCheck(t *testing.T) {
	t.Parallel()

	tcs := []struct {
		payload        string
		user           *user
		expectedErrMsg string
	}{
		{
			expectedErrMsg: "error while reading payload",
		},
		{
			payload:        "{}",
			expectedErrMsg: "not found inside payload",
		},
		{
			payload:        `{"Name":"hello_world"}`,
			user:           &user{RequiredPrefix: faker.Lorem().Word()},
			expectedErrMsg: "which does not have the required prefix",
		},
		{
			payload:        `{"Spec":{"Name": "hello_world"}}`,
			user:           &user{RequiredPrefix: faker.Lorem().Word()},
			expectedErrMsg: "which does not have the required prefix",
		},
		{
			payload: `{"Name":"hello_world"}`,
			user:    &user{RequiredPrefix: "hello"},
		},
		{
			payload: `{"Spec":{"Name": "hello_world"}}`,
			user:    &user{RequiredPrefix: "hello"},
		},
		{
			payload: `{"Name":"hello_world"}`,
			user:    &user{},
		},
		{
			payload: `{"Spec":{"Name": "hello_world"}}`,
			user:    &user{},
		},
	}

	//nolint:paralleltest
	for i, tc := range tcs {
		tc := tc
		t.Run(fmt.Sprintf("run testcase no. %d", i), func(t *testing.T) {
			t.Parallel()
			req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(tc.payload))
			c := echo.New().NewContext(req, httptest.NewRecorder())

			if tc.user != nil {
				c.Set("user", *tc.user)
			}

			err := prefixNameCheck(c)
			if tc.expectedErrMsg == "" {
				require.Nil(t, err)
			} else {
				require.NotNil(t, err)
				require.Contains(t, err.Error(), tc.expectedErrMsg)
			}
		})
	}
}

func TestIsRequestAuthorized(t *testing.T) {
	t.Run("check allow", func(t *testing.T) {
		t.Parallel()

		req := httptest.NewRequest(http.MethodGet, "/_ping", nil)
		c := echo.New().NewContext(req, httptest.NewRecorder())

		ok, err := isRequestAuthorized(c)
		assert.True(t, ok)
		require.Nil(t, err)
	})

	t.Run("check allow", func(t *testing.T) {
		t.Parallel()

		req := httptest.NewRequest(http.MethodDelete, "/_ping", nil)
		c := echo.New().NewContext(req, httptest.NewRecorder())

		ok, err := isRequestAuthorized(c)
		assert.False(t, ok)
		require.NotNil(t, err)
	})
}

func TestPolicyByPath(t *testing.T) {
	t.Parallel()

	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(`{"Name":"hello_world"}`))
	c := echo.New().NewContext(req, httptest.NewRecorder())

	require.NotNil(t, policyByPath("hello", "world")(c))
	require.NotNil(t, policyByPath("_ping", "world")(c))
	require.Nil(t, policyByPath("_ping", http.MethodGet)(c))
}

func TestSanitisePath(t *testing.T) {
	t.Parallel()

	require.Equal(t, "hello", sanitisePath("/hello"))
	require.Equal(t, "world", sanitisePath("/***/world"))
	require.Equal(t, "again", sanitisePath("/at_123abc/again"))
	require.Equal(t, "again", sanitisePath("/at_user:123abc/again"))
}
