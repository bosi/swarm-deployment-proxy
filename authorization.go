package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strings"

	"github.com/labstack/echo/v4"
)

type pathAuthorizer func(c echo.Context) error

const (
	optionalQuery = `(?:\?[\w%\.=_\-]+)?`
	pathPart      = `[\w\/\-:\._]+`
	version       = `v\d+\.\d+`
)

//nolint:lll
var policyMap = map[string]map[string]pathAuthorizer{
	`^_ping$`:                {http.MethodGet: allow, http.MethodHead: allow},
	`^` + version + `/info$`: {http.MethodGet: allow},
	`^` + version + `/auth`:  {http.MethodGet: allow, http.MethodPost: allow},
	`^` + version + `/distribution/[\w\.\/\-\:]+$`: {http.MethodGet: allow},

	`^` + version + `/nodes` + pathPart + `$`:      {http.MethodGet: allow},
	`^` + version + `/nodes` + optionalQuery + `$`: {http.MethodGet: allow},

	`^` + version + `/containers/json$`:                  {http.MethodGet: allow},
	`^` + version + `/containers/` + pathPart + `/json$`: {http.MethodGet: allow},

	`^` + version + `/images/json$`:                  {http.MethodGet: allow},
	`^` + version + `/images/` + pathPart + `/json$`: {http.MethodGet: allow},

	`^` + version + `/plugins/json$`:                  {http.MethodGet: allow},
	`^` + version + `/plugins/` + pathPart + `/json$`: {http.MethodGet: allow},

	`^` + version + `/tasks` + optionalQuery + `$`:             {http.MethodGet: allow},
	`^` + version + `/tasks/` + pathPart + optionalQuery + `$`: {http.MethodGet: allow},

	`^` + version + `/volumes`:                   {http.MethodGet: allow},
	`^` + version + `/volumes/` + pathPart + `$`: {http.MethodGet: allow},

	`^` + version + `/networks` + optionalQuery + `$`:                         {http.MethodGet: allow},
	`^` + version + `/networks/` + pathPart + optionalQuery + `$`:             {http.MethodGet: allow, http.MethodPost: prefixNameCheck, http.MethodDelete: prefixNameCheck},
	`^` + version + `/networks/` + pathPart + `/update` + optionalQuery + `$`: {http.MethodPost: prefixNameCheck},

	`^` + version + `/secrets` + optionalQuery + `$`:                         {http.MethodGet: allow},
	`^` + version + `/secrets/` + pathPart + optionalQuery + `$`:             {http.MethodGet: allow, http.MethodPost: prefixNameCheck, http.MethodDelete: prefixNameCheck},
	`^` + version + `/secrets/` + pathPart + `/update` + optionalQuery + `$`: {http.MethodPost: prefixNameCheck},

	`^` + version + `/configs` + optionalQuery + `$`:                         {http.MethodGet: allow},
	`^` + version + `/configs/` + pathPart + optionalQuery + `$`:             {http.MethodGet: allow, http.MethodPost: prefixNameCheck, http.MethodDelete: prefixNameCheck},
	`^` + version + `/configs/` + pathPart + `/update` + optionalQuery + `$`: {http.MethodPost: prefixNameCheck},

	`^` + version + `/services` + optionalQuery + `$`:                         {http.MethodGet: allow},
	`^` + version + `/services/` + pathPart + optionalQuery + `$`:             {http.MethodGet: allow, http.MethodPost: prefixNameCheck, http.MethodDelete: prefixNameCheck},
	`^` + version + `/services/` + pathPart + `/update` + optionalQuery + `$`: {http.MethodPost: prefixNameCheck},
}

func allow(_ echo.Context) error {
	return nil
}

func deny(c echo.Context) error {
	return fmt.Errorf(
		"access to %s on \"/%s\" is not allowed",
		c.Request().Method,
		sanitisePath(c.Request().URL.Path),
	)
}

func prefixNameCheck(c echo.Context) error {
	data := map[string]interface{}{}
	if c.Request().Method == http.MethodDelete {
		req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://unix%s", c.Param("*")), nil)

		resp, err := dHttpc.Do(req)
		if err != nil {
			return fmt.Errorf("cannot fetch necessary information to determine authorization of delete: %s", err.Error())
		}
		defer resp.Body.Close()

		dec := json.NewDecoder(resp.Body)
		if err := dec.Decode(&data); err != nil {
			return fmt.Errorf("cannot fetch necessary information to determine authorization of delete: %s", err.Error())
		}
	} else {
		d, err := io.ReadAll(c.Request().Body)
		if err != nil {
			return fmt.Errorf("payload must not be empty: %s", err)
		}

		c.Request().Body = io.NopCloser(bytes.NewReader(d))

		if err := json.Unmarshal(d, &data); err != nil {
			return fmt.Errorf("error while reading payload: %s", err)
		}
	}

	name := getName(data)
	if name == "" {
		return errors.New("\"Name\" not found inside payload")
	}

	requiredPrefix := strings.TrimSuffix(c.Get("user").(user).RequiredPrefix, "_") + "_"

	if requiredPrefix != "_" && !strings.HasPrefix(name, requiredPrefix) {
		return fmt.Errorf(
			"you are trying to create a resource with name \"%s\" which does not have the required prefix \"%s\"",
			name,
			requiredPrefix,
		)
	}

	return nil
}

func getName(data map[string]interface{}) string {
	name, ok := data["Name"]
	if ok {
		return name.(string)
	}

	spec, ok := data["Spec"]
	if !ok {
		return ""
	}
	name, ok = spec.(map[string]interface{})["Name"]
	if !ok {
		return ""
	}

	return name.(string)
}

func isRequestAuthorized(c echo.Context) (bool, error) {
	p := policyByPath(c.Request().RequestURI, c.Request().Method)
	err := p(c)
	return err == nil, err
}

func policyByPath(path string, method string) pathAuthorizer {
	path = sanitisePath(path)
	for pattern, methodMap := range policyMap {
		re := regexp.MustCompile(pattern)
		if re.MatchString(path) {
			p, ok := methodMap[method]
			if ok {
				return p
			}
		}
	}

	return deny
}

func sanitisePath(path string) string {
	re := regexp.MustCompile(`\*+|at_.+?/`)
	return strings.Trim(re.ReplaceAllString(path, ""), "/")
}
