package main

import (
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/yaml.v3"
)

type user struct {
	Name           string `json:"name"           yaml:"name"`
	TokenHash      string `json:"tokenHash"      yaml:"tokenHash"`
	RequiredPrefix string `json:"requiredPrefix" yaml:"requiredPrefix"`
}

const (
	requiredTokenParts = 2
)

var (
	config = struct {
		Users []user `json:"users" yaml:"users"`
	}{
		Users: []user{},
	}

	tokenCache     = map[[16]byte]user{}
	tokenCacheLock = &sync.Mutex{}
)

func init() {
	if err := loadUsers(); err != nil {
		panic(err)
	}

	go func() {
		for {
			tokenCacheLock.Lock()
			tokenCache = map[[16]byte]user{}
			tokenCacheLock.Unlock()

			time.Sleep(1 * time.Minute)
		}
	}()
}

func loadUsers() error {
	f, err := os.Open("config.yml")
	if err != nil {
		f, err = os.Open("config.yaml")
	}

	if err == nil {
		dec := yaml.NewDecoder(f)
		if err := dec.Decode(&config); err != nil {
			return fmt.Errorf("cannot parse config as yaml: %s", err)
		}
		return nil
	}

	f, err = os.Open("config.json")
	if err != nil {
		return errors.New("could not find config file (config.yml, config.yaml or config.json)")
	}

	dec := json.NewDecoder(f)
	if err := dec.Decode(&config); err != nil {
		return fmt.Errorf("cannot parse config as json: %s", err)
	}

	return nil
}

func userByToken(token string) (user, bool) {
	tokenCacheLock.Lock()
	defer tokenCacheLock.Unlock()

	tokenDictHash := md5.Sum([]byte(token))
	cachedUser, found := tokenCache[tokenDictHash]
	if found {
		return cachedUser, true
	}

	tokenStripped := strings.TrimPrefix(token, "at_")
	tokenParts := strings.Split(tokenStripped, ":")
	if len(tokenParts) != requiredTokenParts {
		return user{}, false
	}
	userName := tokenParts[0]
	password := tokenParts[1]

	if userName == "" || password == "" {
		return user{}, false
	}

	for _, u := range config.Users {
		if u.Name == userName {
			if bcrypt.CompareHashAndPassword([]byte(u.TokenHash), []byte(password)) == nil {
				tokenCache[tokenDictHash] = u
				return u, true
			}

			return user{}, false
		}
	}

	return user{}, false
}
