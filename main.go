package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var dHttpc http.Client

func init() {
	dHttpc = http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				return net.Dial("unix", "/var/run/docker.sock")
			},
		},
	}
}

func main() {
	log.Fatalln(createRouter().Start(":4242"))
}

func createRouter() *echo.Echo {
	router := echo.New()
	router.GET("status", handleStatus)
	router.Any(":token/*", handleDocker, middleware.Logger(), dockerAuth)

	return router
}

func handleStatus(c echo.Context) error {
	req, _ := http.NewRequest(http.MethodHead, "http://unix/_ping", nil)
	resp, err := dHttpc.Do(req)

	if err != nil || resp.StatusCode != http.StatusOK {
		return c.String(http.StatusInternalServerError, "Docker socket is unreachable")
	}
	defer resp.Body.Close()

	return c.String(http.StatusOK, "App running")
}

func handleDocker(c echo.Context) error {
	path := c.Param("*")
	c.QueryString()

	req, _ := http.NewRequest(c.Request().Method, fmt.Sprintf("http://unix%s?%s", path, c.QueryString()), c.Request().Body)
	for k, vs := range c.Request().Header {
		for _, v := range vs {
			req.Header.Set(k, v)
		}
	}
	resp, err := dHttpc.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	for key, values := range resp.Header {
		for _, value := range values {
			c.Response().Header().Add(key, value)
		}
	}
	return c.Stream(resp.StatusCode, resp.Header.Get("Content-Type"), resp.Body)
}

func dockerAuth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		userToken := c.Param("token")
		if userToken != "" && strings.HasPrefix(userToken, "at_") {
			values := c.ParamValues()
			values[0] = "*****"
			c.SetParamValues(values...)
			c.Request().RequestURI = strings.Replace(c.Request().RequestURI, userToken, "*****", -1)
		}

		u, found := userByToken(userToken)

		if !found {
			return c.String(http.StatusForbidden, "invalid credentials\n")
		}

		c.Set("user", u)

		if ok, err := isRequestAuthorized(c); !ok {
			return c.String(
				http.StatusForbidden,
				fmt.Sprintf(
					"unauthorized: %s\n",
					err,
				),
			)
		}

		return next(c)
	}
}
