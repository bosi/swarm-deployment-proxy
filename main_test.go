package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"

	"github.com/stretchr/testify/require"
)

func TestHandleDocker(t *testing.T) {
	t.Parallel()

	router := createRouter()
	req := httptest.NewRequest(http.MethodGet, "/hello/world", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	require.Equal(t, http.StatusForbidden, rr.Code)
}

//nolint:paralleltest
func TestDockerAuth(t *testing.T) {
	router := echo.New()
	router.Any(":token/*", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	}, dockerAuth)

	pw1, _ := genAndSetTestUser(t)

	tcs := []struct {
		token          string
		method         string
		path           string
		payload        string
		expectedStatus int
		expectedMsg    string
	}{
		{
			method:         http.MethodGet,
			path:           "/something/_ping",
			expectedStatus: http.StatusForbidden,
			expectedMsg:    "invalid credentials",
		},
		{
			method:         http.MethodGet,
			path:           fmt.Sprintf("/at_%s:%s/anypath", config.Users[0].Name, pw1),
			expectedStatus: http.StatusForbidden,
			expectedMsg:    "unauthorized",
		},
		{
			method:         http.MethodGet,
			path:           fmt.Sprintf("/%s:%s/_ping", config.Users[0].Name, pw1),
			expectedStatus: http.StatusForbidden,
			expectedMsg:    "unauthorized",
		},
		{
			method:         http.MethodGet,
			path:           fmt.Sprintf("/at_%s:%s/_ping", config.Users[0].Name, pw1),
			expectedStatus: http.StatusOK,
			expectedMsg:    "ok",
		},
	}

	for i, tc := range tcs {
		t.Run(fmt.Sprintf("run testcase no. %d", i), func(t *testing.T) {
			req := httptest.NewRequest(tc.method, tc.path, nil)
			rr := httptest.NewRecorder()
			router.ServeHTTP(rr, req)
			require.Equal(t, tc.expectedStatus, rr.Code)
			require.Contains(t, rr.Body.String(), tc.expectedMsg)
		})
	}
}
