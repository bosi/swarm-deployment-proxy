**Notice:** this project is a work in progress. Bugs may occur and breaking changes could be made in the future.

# Docker Swarm Deployment Proxy

This project provides an http proxy for the Docker daemon (swarm mode). Its main role is to protect the Docker socket
with token authentication and restrict naming of containers, networks, configurations, etc. It can be used to provide
controlled access to a Docker swarm for multiple teams, where each team can only modify its own services.

## Why, How, What

## Why Have I Started This Project?

I use Docker Swarm to orchestrate multiple servers and services in both personal and commercial contexts. Each project
has an automated deployment that requires management access to the swarm to update a service. My problem with this was
that Docker does not have any type of authorization when accessing a resource. If a user has access to the Docker
socket, he/she can do anything and also accidentally destroy parts of the swarm.

The goal was to create password-protected access to my swarm, including user-specific authorization for deployments.

## How Does the Proxy Work?

Each user is given a unique prefix for their services, networks, etc. When writing resources, the name of the resource
must have this prefix. If the name does not have this prefix, the command is denied. The authentication token is passed
along with the `DOCKER_HOST` environment variable.

## What Does the Proxy?

If the user is authenticated, he/she can read any Docker resource (e.g. `docker ps`, `docker volume ls`
, `docker inspect`, etc.) and create/update/delete services, networks, secrets and configs to which he/she has write
permission.

**Is the proxy bulletproof?** No, definitely not. Docker is not designed for such an authorization concept, so it is
very hard to implement a working solution. There will be bugs and issues, and there may also be cases where the proxy
allows access to the socket when it shouldn't. Please do not use this tool as the only security layer.

## Usage

### Create Configuration

1. create bcrypt password hashes for each user e.g. using apache2-utils: `htpasswd -bnBC 12 "" mypassword | tr -d ':'`
    - Usernames are not further checked for duplicates. If a username is duplicated, only the first user is used to
      check the password.
    - You should choose the bcrypt cost based on the number of users you want to create.
    - It is recommended to just use letters and numbers and at least 20 characters.
    - Users with empty usernames or token hashes will be ignored.
2. create a configuration file in yml or json format (config.yml, config.yaml or config.json):
    ```yaml
    ---
    users:
        -   name: user1
            tokenHash: $2a$12$rpsaCzshUO9RJWtuwy5P4.BXCRNCAENfp6RQR/dxHbLCmxx9DvqX6 # plain: mypassword
            requiredPrefix: test1
        -   name: user2
            tokenHash: $2y$12$PO9T8GE9wmIpViJszBXXy.lSpklCgfPIIZe9i9f48oOUUrfIsAH.q # plain: anotherpassword
            requiredPrefix: test2
    ```

If a user has no required prefix defined he/she has no limitations in updating resources.

### Start the Proxy

#### Run Directly

1. put the config file next to the binary
2. start the proxy: `./swarm-deployment-proxy`

#### Run via Docker Container

1. create stack file named `swarm-proxy.yml` using the content of `docker-compose.yml`
2. run the stack inside your swarm:
   ```shell
   export CHASH=$(md5sum config.yml | cut -d' ' -f1 | tr -d '\n')
   docker stack deploy -c swarm-proxy.yml swarm-deployment-proxy
   ```

Please note that the container runs as root, as access to the Docker socket is required.

### Execute Commands

You may run any docker command: `DOCKER_API_VERSION=1.41 DOCKER_HOST=tcp://127.0.0.1:4242/at_user1:mypassword docker ps`

- Please note that you must prefix your token with `at_`.
- Depending on your setup, there is a known bug where the Docker client cannot retrieve the Docker Api version from the
  daemon. For this reason, the api version is set manually for safety.
- Remember that your token is unencrypted and may show up in the bash history or application logs of reverse proxies.

### Running Commands from Remote

The communication between the Docker daemon and the socket is unencrypted. While there is theoretically the possibility
to use tls for encryption, this is a high overhead and I prefer a different and simpler way.

With ssh you can forward ports over a secured and encrypted connection. All you have to do is open an ssh connection
from your workstation to your server: `ssh -f -N -L 127.0.0.1:4242:127.0.0.1:4242 [user]@[host]`. The example commands
from above are the same.

## Scripts

### Check Swarm Service Update Status

When updating a swarm service with `docker stack deploy`, Docker is only instructed to trigger the update. There is no
easy solution (apart from some sidecar solutions that can be found on Github) to check and wait for an update to
complete. Since I don't want to use an additional service to just check for updates, I wrote a script for this purpose.
You can find it in the `scripts` directory.

Although you can download it directly and run it on your ci/cd, I advise you not to trust me. Look at the script and
create a content hash. Check this hash every time after you download the script to make sure that no malicious or
unwanted commands are executed. Further, pin the version you want with a tag or commit hash (I skip this in the
following example for simplicity reasons).

```bash
curl -lso script.sh https://gitlab.com/bosi/swarm-deployment-proxy/-/raw/main/scripts/wait-for-swarm-service-update.sh
EXPECTED="37e99153ac9e48fbc2625e61d76ab729"
SUM=$(md5sum script.sh | cut -d' ' -f1)
if [[ ${SUM} == ${EXPECTED} ]]; then
    chmod +x script.sh
    ./scrip.sh my_service
    # you may overwrite the default 120s timeout: ./script.sh my_service 90
else
    printf "script content hashes don't match (expect: %s; actual: %s)\n" "${EXPECTED}" "${SUM}"
fi
```

## Used 3rd Party Libraries

This app uses several 3rd party libraries. Their licenses can be
viewed [here](https://bosi.gitlab.io/swarm-deployment-proxy/licenses-3rd-party.txt) (gitlab pages).
